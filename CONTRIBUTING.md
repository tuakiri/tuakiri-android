## Setting up your environment

Before you can compile for Android, you need to setup your environment. This needs to be done only once per system.

 - Install [`rustup`](http://rustup.rs).

 - Install the Java JDK and Ant (on Ubuntu, `sudo apt-get install openjdk-8-jdk ant`)

 - Download and unzip [the Android NDK](http://developer.android.com/tools/sdk/ndk/index.html)
 - Download and unzip [the Android SDK](http://developer.android.com/sdk/index.html) (under *SDK Tools Only* at the bottom)
 - Update the SDK: `./android-sdk-linux/tools/android update sdk -u`

 - Install `cargo-apk` with `cargo install cargo-apk`.
 - Be sure to set `NDK_HOME` to the path of your NDK and `ANDROID_HOME` to the path of your SDK.

## Compiling

In the project root, run `cargo apk`.

This will build an Android package in `target/android-artifacts/build/bin`.

## Testing on an Android emulator

Start the emulator, then run:

```sh
adb install -r target/tuakiri-android
```

This will install your application on the emulator.